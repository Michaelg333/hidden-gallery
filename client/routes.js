Router.map(function(){
    this.route('home', {path: '/'} );
    this.route('entrance', {path: '/lobby'});
    this.route('gallery', {path: '/gallery/:num?',
    onBeforeAction: function () {
       // do some login checks or other custom logic
       var num = this.params.num;
       if (num === undefined){
        num = 1;
       }
       Session.set("gallery", Number(num));
       this.next();
     }});
    this.route('about', {path: '/about'});
    this.route('courtyard', {path: '/courtyard'} );
    this.route('map', {path: '/map'});
    this.route('guestbook', {path: '/guestbook'});
    this.route('guestbooksign', {path: '/guestbooksign'});
    this.route('edit', {path: '/edit'} );   
});

Router.onAfterAction(function() {
        GAnalytics.pageview();

//  analytics.page(this.route.getName());
});

/*
how to create a html link
<a href="{{pathFor 'hello'}}">Anchor</a>
*/


//helper for JavaScript link
/*
if(Meteor.isClient){
    Template.home.home = function(){
        Router.go('home');
    }
}
*/
/*
if(Meteor.isClient){
    Template.entrance.entrance = function(){
        Router.go('entrance');
    }
}

if(Meteor.isClient){
    Template.home.gallery = function(){
        Router.go('gallery');
    }
}

if(Meteor.isClient){
    Template.home.about = function(){
        Router.go('about');
    }
}

if(Meteor.isClient){
    Template.home.courtyard = function(){
        Router.go('courtyard');
    }
}

if(Meteor.isClient){
    Template.home.map = function(){
        Router.go('map');
    }
}

if(Meteor.isClient){
    Template.home.guestbook = function(){
        Router.go('guestbook');
    }
}

if(Meteor.isClient){
    Template.home.guestbooksign = function(){
        Router.go('guestbooksign');
    }
}

if(Meteor.isClient){
    Template.home.edit = function(){
        Router.go('edit');
    }
}

if(Meteor.isClient){
    Template.home.about = function(){
        Router.go('about');
    }
}
*/
