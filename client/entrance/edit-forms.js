Template.newPiece.events ({
  "click #submit-add": function(e,t){
  	e.preventDefault();
  	var title = $("#new-title").val();
  	var url = $("#new-url").val();
  	var medium = $("#new-medium").val();
  	var date = $("#new-date").val();
  	var width = $("#new-width").val();
  	var gallery = $("#new-gallery").val();
  	gallery = Number(gallery);
  	var like = $("#new-like").val();
  	like = Number(like);

  	Art.insert({
		title: title,
		mediaName: url,
		medium: medium,
		date: date,
		width: width,
		gallery: gallery,
		like: like
	}, function(e,id){
		if (e){
			console.log(e);
		}
		else {
			console.log("no error");
		}
	});
  },

})

Template.updatePiece.events ({
	"change select": function(e,t){
		var id = t.$('select').val();
		console.log(id);
		Session.set("editArt", id);
	},
	 "click #submit-update": function(e,t){
	  	e.preventDefault();
	  	var title = $("#update-title").val();
	  	var mediaName = $("#update-url").val();
	  	var medium = $("#update-medium").val();
	  	var date = $("#update-date").val();
	  	var width = $("#update-width").val();
	  	var gallery = $("#update-gallery").val();
	  	gallery = Number(gallery);
	  	var like = $("#update-like").val();
	  	like = Number(like);

	  	Art.update(this._id, {$set:{
			title: title,
			mediaName: mediaName,
			medium: medium,
			date: date,
			width: width,
			gallery: gallery,
			like: like
		}
	}, function(e,id){
		if (e){
			console.log(e);
		}
		else {
			console.log("no error");
		}
	});
  },


})

Template.updatePiece.helpers ({
	artwork: function(){
		return Art.find();
	},
	editArt: function(){
		var art = Session.get("editArt");
		return Art.findOne(art);
	}
})

Template.edit.helpers ({
	loggedIn: function(){
		return !!Session.get("password");
	},
	newmanCount: function(){
		return Session.get("newmanCount") > 3;
	}
})
Template.approveArt.helpers({
	status: function(){
		if (this.approved == "pending"){
			return "pending";
		}
		else if (this.approved){
			return "showing";
		}
		else {
			return "hidden";
		}
	},
	drawing: function(){
		return Drawings.find({},{sort:{date:1}});
	},
	viewDrawing: function(){
		return Drawings.findOne(Session.get("drawingToView"));
	}

})
Template.approveArt.events({
	"click .setShow" : function (e,t){
		Drawings.update( this._id, {$set:{approved:true}});
	},
	"click .setHide" : function (e,t){
		Drawings.update( this._id, {$set:{approved:false}});
	},
	"click .setDelete" : function (e,t){
		if (confirm("Are you sure? Delete foreva :(")) {
   		Drawings.remove( this._id);
		} 
	},
	"click .viewDrawing" : function (e,t){
		Session.set("drawingToView", this._id);
		$(".modal").modal("show");
	}
})

Session.setDefault("newmanCount", 0);
Session.setDefault("password", false);

Template.edit.events ({
	"submit #secureLogIn": function (e,t){
		e.preventDefault();
		var password = t.$("input").val();
		if (password === "tasman123") {
			Session.set("password", true);
		}
		else {
			Session.set("newmanCount", Session.get("newmanCount")+1);
			alert("wrong password!")
		}
	}

})