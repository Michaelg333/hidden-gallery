Template.footer.helpers ({
  page : function(p){
    return Router.current().route.getName()=== p;
  },
  pageOr : function(a,b){ 
    return Session.equals('page',a)|| Session.equals('page',b)
 }
})



Template.footer.events ({
  "click .home-link": function(e,t){
    Router.go("home");
  },
    "click .entrance-link": function(e,t){
    Router.go("entrance");
  },
  	"click .map-link": function(e,t){
  		Router.go("map");
  	},
    "click .courtyard-link": function(e,t){
    Router.go("courtyard");
  },
    "click .about-link": function(e,t){
      Router.go("about");
  },
    "click .guestbook-link": function(e,t){
    Router.go("guestbook");
  },
    "click .gallery-link": function(e,t){
    Router.go("gallery");
  },
    "click .next-link": function(e,t){
    var currentGallery = Number(Session.get("gallery"));
    if (currentGallery === 5) {
    	Router.go('entrance');
    } else {
    var nextGallery = Number(currentGallery) + 1;
    var url = '/gallery/' + nextGallery;
    Router.go(url);
    console.log(url);
    $(".gallery").scrollLeft(0)
	};	
  },
  "click .last-link": function(e,t){
  	var currentGallery = Number(Session.get("gallery"));
  	if (currentGallery === 1) {
  		Router.go('entrance');
  	} else {
  	var lastGallery = Number(currentGallery) - 1;
    var url = '/gallery/' + lastGallery;
  	Router.go(url);
  	console.log(url);
    $(".gallery").scrollLeft(0)
  };
  }
})