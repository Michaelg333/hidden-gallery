Template.courtyardLayers.rendered = function(){ 
     var scene = $("#myParallax").parallax(); 
     $scene.parallax('enable');
     //scene.parallax('disable');
     $scene.parallax('calibrate', false, true);
     $scene.parallax('invert', false, true);
     $scene.parallax('limit', false, 10);
     $scene.parallax('scalar', 2, 8);
     $scene.parallax('friction', 0.2, 0.8);  
};