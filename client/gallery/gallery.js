Template.gallery.helpers ({
	galleryWidth: function() {

		// var width;
		//count all the sidths of the pictures loaded and return sum
		// width = "2800px";

// so you need to load up all the relevant pictures
// that will be the same code for which photos to show
// something like...
// var gallery = Session.get("gallery");
// artPieces = Art.find({gallery: gallery}).fetch()
// var artPieces
// then you have all the artPieces as an array
// then
// itereate over that array
// each time adding up the widths of the pieces, and adding the padding you want
// you'll use underscore
// var width = 0;
// _.each(THE ARRAY, function(a){
// the function here. you can access the current art pieces with a. so a.width is the width of tha tpiece.  you can then just do something like 
// width = width+ a.width + PADDING;
// })
		var width = this.width;
		var mq = window.matchMedia( "(min-width: 500px)" )
		var gallery = Session.get("gallery");
		artPieces = Art.find({gallery: gallery}).fetch()
		var artPieces;
		var width = 0;
		_.each(artPieces, function(a){
			width = width + parseInt(a.width) + 170;
		})
		if (mq.matches) {
			width = width + "px";
			return width;
		}			
		else {
			width = width/2 + "px";
			return width;
		}	
	},

	art: function() {
		var galleryNumber = Session.get("gallery") 
		return Art.find({gallery:galleryNumber});
	},

	galNum: function() {
		return Session.get("gallery");
	},	
	disableLeft : function() {
			return (Session.equals("scrollLeft", 0)) ? "disabled" : false;
	},
	disableRight : function (){
		var scrollLeft = Session.get("scrollLeft");
		if (!!$(".line").css("width")){
		var maxScroll = $(".line").css("width").split("px")[0] - $(".gallery").css("width").split("px")[0];
		return (Session.equals("scrollLeft", maxScroll) ) ? "disabled" : false;}
	},
		mobile : function(){
		var mq = window.matchMedia( "(max-width: 1024px)" )
        if (mq.matches) {
        	return true;
      	} else {
      		return false;	
      	}
	}
})

Template.artPiece.helpers ({
	liked: function() {
		var likedArt = Session.get("ulikedArt");
		console.log(likedArt)
		if(_.contains(likedArt, this._id)) {
  			return "liked";
  		}
	},
	width: function(){
		var width = this.width;
		var mq = window.matchMedia( "(min-width: 500px)" )
		if (mq.matches) {
		return width;
		}			
		else {
			console.log("small")
		return width.slice(0,width.length-2)/2+"px";
		}
	}
})


Template.gallery.events({
  'click .glyphicon-thumbs-up': function () {
  	//check array
  	var likedArt = Session.get("ulikedArt");
  	if(_.contains(likedArt, this._id)) {
  		return false;
  	}
  	else{
	  	var count = (!!this.like)?this.like:0;
		var likeCount = count + 1;
		//update database
		console.log("like button worked", likeCount)

		Art.update(this._id, {$set:{
			like: likeCount
		}});
		//if else add id to array

		likedArt.push(this._id)
		Session.set("ulikedArt", likedArt)
	}
  },
    "click .artzoom-link": function(e,t){
    Session.set("artzoom", this._id);
    $(".modal").modal();
  },
  "mouseenter .slide-left": function(){
    Session.set("slideLeft", true);
  },
  "mouseleave .slide-left": function(){
    Session.set("slideLeft", false);
  },
   "mouseenter .slide-right": function(){
    Session.set("slideRight", true);
  },
  "mouseleave .slide-right": function(){
    Session.set("slideRight", false);
  },
});


Template.gallery.onRendered (function(){
	$(".gallery").on("scroll", function(){
		var currentLeft = $(".gallery").scrollLeft();
		Session.set("scrollLeft", currentLeft)	
	});
});

Template.about.events({
	"click .guestbook-link": function(e,t){
    Router.go("guestbook");
  },
})

Template.artzoom.helpers ({
	artzoom: function() {
		return Art.findOne(Session.get("artzoom"));
	},
	url : function(){
		console.log(this.url)
		return this.url.toLowerCase();
	},
	mobile : function(){
		var mq = window.matchMedia( "(max-width: 1024px)" )
        if (mq.matches) {
        	return true;
      	} else {
      		return false;	
      	}
	}

})
Template.modal.helpers ({
	mobile : function(){
		var mq = window.matchMedia( "(max-width: 1024px)" )
        if (mq.matches) {
        	return true;
      	} else {
      		return false;	
      	}
	}
})
Template.modal.events({
	"click .modal-backdrop" : function(){
		Session.set("artzoom", false);
	}
})

Tracker.autorun(function(){
	var currentLeft;
var distance = 40;
var time = 50; //ms

if( Session.get("slideLeft") ) {
    var leftInterval = Meteor.setInterval(function(){
        if(!Session.get("slideLeft")) {
            Meteor.clearInterval(leftInterval);
            return false;
        }
        currentLeft = $(".gallery").scrollLeft();
        if(currentLeft > 0 ) {
            var newLeft = currentLeft-distance;
            $(".gallery").scrollLeft(newLeft);
                	//$( "button.slide-left" ).removeClass( "disabled" );

        } else {
        	//$( "button.slide-left" ).addClass( "disabled" );
        }
    },time);
}

//for slide right
if( Session.get("slideRight") ) {
    var rightInterval = Meteor.setInterval(function(){
        if(!Session.get("slideRight")) {
            Meteor.clearInterval(rightInterval);
            return false;
        }
        currentLeft = $(".gallery").scrollLeft();
        var newLeft = currentLeft+distance;
        $(".gallery").scrollLeft(newLeft);
        
    },time);
}

})
